<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Laravel log viewer</title>
    <link rel="stylesheet"
          href="{{asset('assets/styles/vendor/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
    <style>
        body {
            padding: 25px;
        }

        h1 {
            font-size: 1.5em;
            margin-top: 0;
        }

        #table-log {
            font-size: 0.85rem;
        }

        .sidebar {
            font-size: 0.85rem;
            line-height: 1;
        }

        .btn {
            font-size: 0.7rem;
        }

        .stack {
            font-size: 0.85em;
        }

        .date {
            min-width: 75px;
        }

        .text {
            word-break: break-all;
        }

        a.llv-active {
            z-index: 2;
            background-color: #f5f5f5;
            border-color: #777;
        }

        .list-group-item {
            word-wrap: break-word;
        }

        .folder {
            padding-top: 15px;
        }

        .div-scroll {
            height: 80vh;
            overflow: hidden auto;
        }

        .nowrap {
            white-space: nowrap;
        }

        .level svg {
            margin-right: 5px;
        }

        .level-select select {
            display: inline-block;
            margin-left: 5px;
            width: auto;
        }

    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <div class="col sidebar mb-3">
            <h1><i class="fa fa-calendar" aria-hidden="true"></i> Laravel Log Viewer</h1>
            <p class="text-muted"><i>Server Time: </i><i id="clock">{{$serverTime}}</i></p>
            <div class="list-group div-scroll">
                @foreach($folders as $folder)
                    <div class="list-group-item">
                        <a href="?f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}">
                            <span class="fa fa-folder"></span> {{$folder}}
                        </a>
                        @if ($current_folder == $folder)
                            <div class="list-group folder">
                                @foreach($folder_files as $file)
                                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}&f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}"
                                       class="list-group-item @if ($current_file == $file) llv-active @endif">
                                        {{$file}}
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
                @foreach($files as $file)
                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
                       class="list-group-item @if ($current_file == $file) llv-active @endif">
                        {{$file}}
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-10 table-container">
            @if ($logs === null)
                <div>
                    Log file >50M, please download it.
                </div>
            @else
                <table id="table-log" class="table table-striped" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
                    <thead>
                    <tr>
                        @if ($standardFormat)
                            <th>Level</th>
                            <th>Context</th>
                            <th>Date</th>
                        @else
                            <th>Line number</th>
                        @endif
                        <th>Content</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($logs as $key => $log)
                        <tr data-display="stack{{{$key}}}">
                            @if ($standardFormat)
                                <td class="nowrap text-{{{$log['level_class']}}}">
                                    <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span>{{$log['level']}}
                                </td>
                                <td class="text">{{$log['context']}}</td>
                            @endif
                            <td class="date">{{{$log['date']}}}</td>
                            <td class="text">
                                @if ($log['stack'])
                                    <button type="button"
                                            class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2  exp-log"
                                            data-display="stack{{{$key}}}">
                                        <span class="fa fa-search"></span>
                                    </button>
                                @endif
                                {{{$log['text']}}}
                                @if (isset($log['in_file']))
                                    <br/>{{{$log['in_file']}}}
                                @endif
                                @if ($log['stack'])
                                    <div class="stack" id="stack{{{$key}}}"
                                         style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif
            <div class="p-3">
                @if($current_file)
                    <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-download"></span> Download file
                    </a>
                    -
                    <a id="clean-log" href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-sync"></span> Clean file
                    </a>
                    -
                    <a id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-trash"></span> Delete file
                    </a>
                    @if(count($files) > 1)
                        -
                        <a id="delete-all-log" href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                            <span class="fa fa-trash-alt"></span> Delete all files
                        </a>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
<!-- jQuery for Bootstrap -->

<script src="{{ asset('assets/js/vendor/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.bundle.min.js')}}"></script>
<!-- FontAwesome -->
<script defer src="{{asset('assets/js/vendor/fontawesome/all.js')}}"></script>
<!-- Datatables -->
<script type="text/javascript" src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
{{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>--}}
<script>
    $(document).ready(function () {
        $('.table-container tr .exp-log').on('click', function () {
            $('#' + $(this).data('display')).toggle();
        });

        var options = {
            "order": [$("#table-log").data("orderingIndex"), "desc"],
            "stateSave": true,
            "stateSaveCallback": function (settings, data) {
                window.localStorage.setItem("datatable", JSON.stringify(data));
            },
            "stateLoadCallback": function (settings) {
                var data = JSON.parse(window.localStorage.getItem("datatable"));
                if (data) data.start = 0;
                return data;
            },
        };

        @if ($standardFormat)
            options.dom = "<'row'<'col-sm-4'l><'level-select col-sm-4'><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";
                @endif

        var table = $("#table-log").DataTable(options);

        $("#delete-log, #clean-log, #delete-all-log").click(function () {
            return confirm("Are you sure?");
        });

                {{-- Add a level select filter --}}
                @if ($standardFormat)
        var levels = @json($levels);
        levels.unshift('DANGER', 'INFO', 'WARNING');
        var level = @json($level);
        var groups = @json($groups);

        var select = $('<select class="form-control form-control-sm" id="level"><option value="">all</option></select>');
        for (var i = 0; i < levels.length; i++) {

            var option = $('<option></option>');
            option.attr('value', levels[i]);
            option.text(' ' + levels[i] + ' ');
            if(levels[i] == level){
                option.attr('selected', 'selected');
            }
            select.append(option);
        }
        $("div.level-select").append('<label>Level </label>');
        $("div.level-select label").append(select);


        // Redraw the table whenever the level select changes
        $("#level").change(function () {
            setCookie('forLogs', $(this).val(), 999999)
            table.draw();
        });

        $.fn.dataTable.ext.search.push(
            function (settings, data) {
                var level = $("#level").val();
                switch (level) {
                    case 'DANGER':
                        return groups.danger.includes(data[0].trim())
                        break;
                    case 'WARNING':
                        return groups.warning.includes(data[0].trim())
                        break;
                    case 'INFO':
                        return groups.info.includes(data[0].trim())
                        break;
                    default:
                        return !level || data[0].trim() === level;
                        break;
                }

            },
        );
        @endif

    });
    function setCookie(cname, cvalue, exminutes) {
        var d = new Date();
        d.setTime(d.getTime() + (exminutes * 60 * 1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/logs";
    }

    function increment_time_element(element, delay) {
        var interval, last,
            time_pattern = /(\d+):(\d+):(\d+)/,
            start = element.innerHTML.match(time_pattern),
            then = new Date;

        then.setHours  (parseInt(start[1], 10) || 0);
        then.setMinutes(parseInt(start[2], 10) || 0);
        then.setSeconds(parseInt(start[3], 10) || 0);

        function now() {
            return Date.now ? Date.now() : (new Date).getTime();
        }

        last = now();

        interval = setInterval(function () {
            var current = now();
            // correct for any interval drift by using actual difference
            then.setTime(then.getTime() + current - last)
            last = current;
            element.innerHTML = then.toString().match(time_pattern)[0];
        }, delay || 1000);

        return {cancel: function() { clearInterval(interval) }};
    }

    // Then, if you want to cancel:
    //incrementing_time.cancel();
    if(document.readyState === 'ready' || document.readyState === 'complete') {
        $("#level").change();
        var incrementing_time =
            increment_time_element(document.getElementById("clock"));

    } else {
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                $("#level").change();
                var incrementing_time =
                    increment_time_element(document.getElementById("clock"));

            }
        }
    }
</script>
</body>
</html>
